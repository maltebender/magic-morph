CUDA_VISIBLE_DEVICES=$4
cd glow-master/demo
python3 new.py $1 $2
cd ../../dcscn-super-resolution
python3 scale1.py $1 $2 --scale=4 --file=".test.png"
python3 scale2.py $1 $2 $3 --scale=2 --file=".test.png"
cd ../
outFolder="../$2"
outFile="../$2.tar.gz"
tar -zcvf $outFile $outFolder
import sys
import tensorflow as tf

import DCSCN
from helper import args

from shutil import copyfile
import shutil
import os
from pathlib import Path
import datetime



rootdir = Path('./../../'+sys.argv[2])

clip_list = [f for f in rootdir.glob('*') if f.is_dir()]
clip_list.sort()


args.flags.DEFINE_string("file", "image.jpg", "Target filename")
FLAGS = args.get()


def main(_):
	model = DCSCN.SuperResolution(FLAGS, model_name=FLAGS.model_name)
	model.build_graph()
	model.build_optimizer()
	model.build_summary_saver()

	model.init_all_variables()
	model.load_model()


	for f in clip_list:
		
		sub_list = [f for f in f.glob('*') if f.is_dir()]
		sub_list.sort()

		for sf in sub_list:

			seqdir = Path('./'+str(sf)+'/seq_4x')
			tmpDir = Path('./'+str(sf)+'/tmp/')
			outDir = Path('./'+str(sf)+'/seq/')

			if os.path.isdir(str(tmpDir)):
				shutil.rmtree(str(tmpDir))

			if os.path.isdir(str(outDir)):
				shutil.rmtree(str(outDir))

			os.mkdir(str(tmpDir))
			os.mkdir(str(outDir))

			frames_list = [seqdir for seqdir in seqdir.glob('*.png') if seqdir.is_file()]

			for frame in frames_list:
				
				if not sys.argv[3] == 'lowres':
					model.do_for_file(str(frame), str(tmpDir))
					frameOut = str(outDir)+'/'+frame.parts[-1]
					newFileName = frame.parts[-1].replace('.png','_result.png')
					resultImg = str(tmpDir)+'/dcscn_L12_F196to48_NIN_A64_PS_R1F32/'+newFileName
					shutil.move(resultImg ,frameOut)

				if sys.argv[3] == 'lowres':
					frameOut = str(outDir)+'/'+frame.parts[-1]
					shutil.move(str(frame) ,frameOut)

			if os.path.isdir(str(tmpDir)):
				shutil.rmtree(str(tmpDir))

			if os.path.isdir(str(seqdir)):
				shutil.rmtree(str(seqdir))


if __name__ == '__main__':
	tf.app.run()

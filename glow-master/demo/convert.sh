mkdir ./convert
mkdir ./convert/seq
cp ./convert_base/Balenciaga_VH_LD_EDIT_0.png ./convert/from.png
cp ./convert_base/Balenciaga_VH_LD_EDIT_1.png ./convert/to.png
python3 convert.py
mv ./convert ./out/clip1

mkdir ./convert
mkdir ./convert/seq
cp ./convert_base/Balenciaga_VH_LD_EDIT_1.png ./convert/from.png
cp ./convert_base/Balenciaga_VH_LD_EDIT_2.png ./convert/to.png
python3 convert.py
mv ./convert ./out/clip2

mkdir ./convert
mkdir ./convert/seq
cp ./convert_base/Balenciaga_VH_LD_EDIT_2.png ./convert/from.png
cp ./convert_base/Balenciaga_VH_LD_EDIT_3.png ./convert/to.png
python3 convert.py
mv ./convert ./out/clip3

mkdir ./convert
mkdir ./convert/seq
cp ./convert_base/Balenciaga_VH_LD_EDIT_3.png ./convert/from.png
cp ./convert_base/Balenciaga_VH_LD_EDIT_4.png ./convert/to.png
python3 convert.py
mv ./convert ./out/clip4
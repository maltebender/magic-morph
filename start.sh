aniCount=4
frameCount=120
imgName="Balenciaga_VH_LD_EDIT_"
imgExtension=".png"

if [ -d "./glow-master/demo/out/clip1" ]; then
  rm -r ./glow-master/demo/out/*
fi

if [ -d "./glow-master/demo/convert" ]; then
  rm -r ./glow-master/demo/convert
fi

if [ -d "./out" ]; then
  rm -r ./out
fi

mkdir ./out

for (( i=0; i<${aniCount}; i++ ));
do

	n1=$(($i+1))
	f1="./glow-master/demo/convert_base/$imgName$i$imgExtension"
	f2="./glow-master/demo/convert_base/$imgName$n1$imgExtension"
	clip="clip$i"

	mkdir ./glow-master/demo/convert
	mkdir ./glow-master/demo/convert/seq
	cp $f1 ./glow-master/demo/convert/from.png
	cp $f2 ./glow-master/demo/convert/to.png
	cd ./glow-master/demo/
	python3 new.py
	cd ..
	cd ..
	mv ./glow-master/demo/convert "./glow-master/demo/out/$clip"

	mkdir "./out/$clip"


	for (( o=0; o<${frameCount}; o++ ));
	do

		cd dcscn-super-resolution
		python3 sr.py --scale=4 --file="../glow-master/demo/out/${clip}/seq/${o}.png"
		python3 sr.py --scale=2 --file="./output/dcscn_L12_F196to48_Sc4_NIN_A64_PS_R1F32/${o}_result.png"
		cd ..

		cp "./dcscn-super-resolution/output/dcscn_L12_F196to48_NIN_A64_PS_R1F32/${o}_result_result.png" "./out/${clip}/${o}.png"
		rm -r ./dcscn-super-resolution/output/*

	done

done